## joern's Readme

Hello there, I'm Jörn but in most places online it type out my name either `joern` or use my handle `joernchen`.

I'm a Principal Security Engineer in [GitLab's Security Research](https://handbook.gitlab.com/handbook/security/product-security/security-research/) team.

Please note: This Readme is very much work in progress it'll hopefully get more content eventually.

### Working with me

I'm usually very direct when it comes to giving feedback. I expect the same from you,
no one needs to take a detour: please just leave your honest feedback with me directly.

### My working hours

My personal situation is pretty unique right now, which is why I'm working this strange ["non-linear"](https://handbook.gitlab.com/handbook/company/culture/all-remote/non-linear-workday/#q-what-is-a-non-linear-workday-routine) schedule.
Sometimes you'll see me active late at night or on weekends, and other times I might be offline during standard hours.
I heavily rely on this type of work and am grateful that I can do so at GitLab.
This is just what works for my life at the moment - definitely not something which is expected from any of you!
Please don't feel like you need to respond to my off-hours messages or match my weekend work. 
I'm not trying to set any kind of precedent here.

Just doing what I need to do to make things work on my end.